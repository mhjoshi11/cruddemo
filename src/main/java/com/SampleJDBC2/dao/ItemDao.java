package com.SampleJDBC2.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.SampleJDBC2.model.Item;
import com.SampleJDBC2.util.JDBConnection;

public class ItemDao {
	private PreparedStatement pstmt = null;

	Connection conn = JDBConnection.getDBConnection();

	public int insertItem(Item item1) throws SQLException {
		pstmt = conn.prepareStatement("insert into item(name,price) values (?, ?)");
		pstmt.setString(1, item1.getName());
		pstmt.setFloat(2, item1.getPrice());
		int r = pstmt.executeUpdate();
		return r;
		
	}

	public int deleteItem(String pname) throws SQLException {
		pstmt = conn.prepareStatement("delete from item where name=?;");
		pstmt.setString(1, pname);
		int r = pstmt.executeUpdate();
		return r;
	}

	public int getAllItems() throws SQLException {
		Statement st = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
		ResultSet rs = st.executeQuery("select * from item");
		System.out.println("...........................................");
		while (rs.next()) {
			int id = rs.getInt("id");
			String name = rs.getString("name");
			float price = rs.getFloat("price");

			System.out.println("ID: " + id + " name: " + name + " price: " + price);
			System.out.println("...........................................");

		}
		if (rs.last()) {
			return rs.getRow();
		} else {
			return 0; // just cus I like to always do some kinda else statement.
		}
	}
}
