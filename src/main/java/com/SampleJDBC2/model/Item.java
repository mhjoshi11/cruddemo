package com.SampleJDBC2.model;

import lombok.Data;

@Data
public class Item {
	
	private int id;
	private String name;
	private float price;

}
