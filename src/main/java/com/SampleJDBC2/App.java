package com.SampleJDBC2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import com.SampleJDBC2.dao.ItemDao;
import com.SampleJDBC2.model.Item;

/**
 * Hello world!
 *
 */
public class App {
	static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

	private static Item getItem() {
		Item item1 = new Item();
		try {
//			System.out.println("Enter id: ");
//			int id = Integer.parseInt(br.readLine());
			System.out.println("Enter Item Name: ");
			String name = br.readLine();
			System.out.println("Enter Item Price: ");
			float price = Float.parseFloat(br.readLine());

//			item1.setId(id);
			item1.setName(name);
			item1.setPrice(price);

		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return item1;

	}

	private static String getName() {
		String pname = null;
		try {
			System.out.println("Enter Item Name: ");
			pname = br.readLine();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return pname;
	}

	public static void main(String[] args) throws SQLException {
		ItemDao itemDao = new ItemDao();

		boolean flag = true;
		while (flag) {

			System.out.println("******----Welcome to Sample Project---******");
			System.out.println("Press 1 for add new Item");
			System.out.println("Press 2 for delete Item");
			System.out.println("Press 3 for display all Items");

			try {
				System.out.println("Enter your Choice: ");
				int n = Integer.parseInt(br.readLine());

				switch (n) {
				case 1: // Add new Item
					Item item1 = getItem();
					int r = itemDao.insertItem(item1);
					System.out.println(r + "Data inserted Successfully");
					break;
				case 2:// delete Item
					String pname = getName();
					int r1 = itemDao.deleteItem(pname);
					if (r1 > 0)
						System.out.println(r1 + " Success");
					else
						System.out.println("Error");
					break;
				case 3:// display all Items
					int size=itemDao.getAllItems();
					System.out.println("No. of rows: "+size);
					break;
				case 4:// exit
					flag = false;
					break;

				default:
					break;
				}
			} catch (NumberFormatException e) {
				System.out.println("Type valid number");
				e.printStackTrace();
			} catch (IOException e) {
				System.out.println("Invalid Input");
				e.printStackTrace();
			}

		}

	}
}