package com.SampleJDBC2;

import org.testng.annotations.Test;

import com.SampleJDBC2.dao.ItemDao;
import com.SampleJDBC2.model.Item;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.testng.Assert;
import org.testng.AssertJUnit;

/**
 * Unit test for simple App.
 */
public class AppTest {

	ItemDao itemDao = new ItemDao();
	Item item = new Item();

	public AppTest(String testName) {
	}

	@Test
	public void testInsertData() throws SQLException {
		item.setName("WaterBottle");
		item.setPrice(500);
		int r = itemDao.insertItem(item);
		Assert.assertEquals(r, 1);
	}

	@Test(dependsOnMethods = { "testInsertData" })
	public void testDeleteData() throws SQLException {
		String name = "WaterBottle";
		int r = itemDao.deleteItem(name);
		Assert.assertEquals(r, 1);
	}

	@Test(dependsOnMethods = { "testInsertData", "testDeleteData" })
	public void testGetAllItems() throws SQLException {
		int size = itemDao.getAllItems();
		Assert.assertEquals(size, 4);
	}

	@Test
	public void testApp() {
		AssertJUnit.assertTrue(true);
	}
}
